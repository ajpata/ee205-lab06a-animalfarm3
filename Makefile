###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 06a - Animal Farm 3
#
# @file    Makefile
# @version 1.0
#
# @author AJ Patalinghog <ajpata@hawaii.edu>
# @brief  Lab 05a - Animal Farm 2 - EE 205 - Spr 2021
# @date   11 March 2021
###############################################################################

all: main

main.o:  animal.hpp main.cpp
	g++ -c main.cpp

animal.o: animal.hpp animal.cpp
	g++ -c animal.cpp

animalFactory.o: animalFactory.hpp animalFactory.cpp
	g++ -c animalFactory.cpp

mammal.o: mammal.hpp mammal.cpp animal.hpp
	g++ -c mammal.cpp

cat.o: cat.cpp cat.hpp mammal.hpp
	g++ -c cat.cpp

dog.o: dog.cpp dog.hpp mammal.hpp
	g++ -c dog.cpp

fish.o: fish.hpp fish.cpp animal.hpp
	g++ -c fish.cpp

nunu.o: nunu.hpp nunu.cpp fish.hpp
	g++ -c nunu.cpp

aku.o: aku.hpp aku.cpp fish.hpp
	g++ -c aku.cpp

bird.o: bird.hpp bird.cpp animal.hpp
	g++ -c bird.cpp

palila.o: palila.hpp palila.cpp bird.hpp
	g++ -c palila.cpp

nene.o: nene.hpp nene.cpp bird.hpp
	g++ -c nene.cpp

main: main.cpp *.hpp main.o animal.o animalFactory.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
	g++ -o main main.o animal.o animalFactory.o mammal.o cat.o dog.o fish.o nunu.o aku.o bird.o palila.o nene.o
	
clean:
	rm -f *.o main
